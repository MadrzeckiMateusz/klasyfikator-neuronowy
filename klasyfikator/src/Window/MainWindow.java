package Window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;



import Data.LoadNames;
import Data.XmlNetworkStructReader;
import Data.XmlNetworkStructWriter;
import Dialogs.NewNetworkDialog;
import Network.TrainNetwork;

public class MainWindow extends JFrame implements ActionListener, InternalFrameListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Dimension DimensionOfFrame;
	private Dimension DimensionOfScreenSize;
	private String [] MenuName = {"Plik","Struktura sieci","Opcje sieci","Pomoc"};
	private String [] MenuItem = {"Nowa sie�", "Za�aduj Dane","Zako�cz","Zmie�","Zapisz","Wczytaj","Trenuj","Rozpoznaj","Konspekt pomocy","O programie"};
	private String [] Tooltiptext = {"Nowa sie�", "Za�aduj dane", "Trenuj Sie�", "Klasyfikuj","Zapisz struktur�","Wczytaj struktur�","Pomoc programowa"};
	private JMenuBar MenuBar;
	private JMenu	FileMenu;
	private JMenu	StructMenu;
	private JMenu	OptionMenu;
	private JMenu	HelpMenu;
	private JMenuItem FNewNet;
	private JMenuItem FLoadData;
	private JMenuItem FClose;
	private JMenuItem SSave;
	private JMenuItem SRead;
	private JMenuItem OTrain;
	private JMenuItem OMacht;
	private JMenuItem Hkonsp;
	private JMenuItem HAbout;
	
	private JButton newNetworkBt;
	private JButton LoadDataBt;
	private JButton TrainBt;
	private JButton MachtBt;
	private JButton LoadBt;
	private JButton Savebt;
	private JButton Helpbt;
	
	private JLabel statusBar;
	
	
	private JPanel ButtonPanel;
	
	private JDesktopPane deskpane;
	
	
	private NewNetworkDialog newnetworkDialog;
	private JFileChooser filechooser;
	private JFileChooser filechoosername;
	private LoadNames loadnames;
	
	
	private ArrayList<InternalWindow> internalFrames;
	private ArrayList<TrainNetwork>	NetworkSet;
	
	private Container contentpane;

	private int numofcurent=0;
	private int numofNet=0;
	

	public MainWindow(){
		
	}
	public MainWindow(boolean visible){
		
		this.internalFrames = new ArrayList<InternalWindow>();
		this.NetworkSet = new ArrayList<TrainNetwork>();
		this.newnetworkDialog = new NewNetworkDialog();
		this.filechooser = new JFileChooser();
		this.filechoosername = new JFileChooser();
		this.deskpane = new JDesktopPane();
		this.loadnames = new LoadNames();
		this.deskpane.setBackground(Color.LIGHT_GRAY);
		
		
		
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				MainWindow.this.CloseWindow();
			}
		});
		try {
		UIManager.setLookAndFeel
		("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
		}
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.DimensionOfFrame = new Dimension(1000,700);		
		this.DimensionOfScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
		if (DimensionOfFrame.height > this.DimensionOfScreenSize.height){
			DimensionOfFrame.height = DimensionOfScreenSize.height;
		}
		if (DimensionOfFrame.width > DimensionOfScreenSize.width){
			DimensionOfFrame.width = DimensionOfScreenSize.width;
		}
		setSize(DimensionOfFrame);
		setLocation((DimensionOfScreenSize.width - DimensionOfFrame.width) / 2, 
			      (DimensionOfScreenSize.height - DimensionOfFrame.height) / 2);
		setTitle("Klasyfikator");
		setIconImage((new ImageIcon(MainWindow.class.getResource("/resources/progic.png")).getImage()));
		setResizable(false);
		this.contentpane = this.getContentPane();
		setLayout(new BorderLayout());
		CreateGui();
		CreateMenu();
		CreateButoonPanel();
		setVisible(visible);
		
	
	}
	public void CreateGui(){
		
		this.statusBar = new JLabel();
		this.statusBar.setBorder(BorderFactory.createEtchedBorder());
		SetMaxSize(new Dimension(20,20), this.statusBar);
		this.contentpane.add(this.deskpane,"Center");
		this.filechooser.updateUI();
		contentpane.add(this.statusBar, BorderLayout.PAGE_END);
	}
	
	public void CreateMenu(){
		this.MenuBar = new JMenuBar();
		this.MenuBar.setSize(1000, 80);
		
		this.FileMenu = this.CreateMenu(this.MenuName[0], KeyEvent.VK_P,this.MenuBar);
		this.StructMenu = this.CreateMenu(this.MenuName[1], KeyEvent.VK_T,this.MenuBar);
		this.OptionMenu = this.CreateMenu(this.MenuName[2], KeyEvent.VK_O,this.MenuBar);
		this.HelpMenu = this.CreateMenu(this.MenuName[3], KeyEvent.VK_M,this.MenuBar);
		
		this.FNewNet = this.CreateJMenuItem(this.MenuItem[0], KeyStroke.getKeyStroke(78,8), this.FileMenu,"/resources/nowa24.png");
		this.FLoadData = this.CreateJMenuItem(this.MenuItem[1], KeyStroke.getKeyStroke(76, 8), this.FileMenu,"/resources/zaladuj24.png");
		this.FileMenu.addSeparator();
		this.FClose = this.CreateJMenuItem(this.MenuItem[2], KeyStroke.getKeyStroke(90,8), this.FileMenu,"/resources/zamknij24.png");
		
		
		this.SSave = this.CreateJMenuItem(this.MenuItem[4], KeyStroke.getKeyStroke(83,8), this.StructMenu,"/resources/save24.png");
		this.SRead = this.CreateJMenuItem(this.MenuItem[5], KeyStroke.getKeyStroke(87,8), this.StructMenu,"/resources/wczytaj24.png");
		
		this.OTrain = this.CreateJMenuItem(this.MenuItem[6], KeyStroke.getKeyStroke(85,8), this.OptionMenu,"/resources/trenuj24.png");
		this.OptionMenu.addSeparator();
		this.OMacht = this.CreateJMenuItem(this.MenuItem[7], KeyStroke.getKeyStroke(82,8), this.OptionMenu,"/resources/rozpoznaj24.png");
		
		
		this.Hkonsp = this.CreateJMenuItem(this.MenuItem[8], KeyStroke.getKeyStroke(72,8), this.HelpMenu,"/resources/help24.png");
		this.HAbout = this.CreateJMenuItem(this.MenuItem[9], KeyStroke.getKeyStroke(65,8), this.HelpMenu,"/resources/author24.png");
		
		setJMenuBar(this.MenuBar);
	}
	
	public void CreateButoonPanel(){
		this.ButtonPanel = new JPanel();
		this.ButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER,5,5));
		this.ButtonPanel.setBorder(BorderFactory.createEtchedBorder());
		SetMaxSize(new Dimension(68,500), this.ButtonPanel);
		
		this.newNetworkBt = createButton("/resources/nowa48.png", new Dimension(58, 58),this.Tooltiptext[0]);
		this.LoadDataBt = createButton("/resources/zaladuj48.png", new Dimension(58, 58), this.Tooltiptext[1]);
		this.TrainBt = createButton("/resources/trenuj48.png", new Dimension(58, 58), this.Tooltiptext[2]);
		this.MachtBt = createButton("/resources/rozpoznaj48.png", new Dimension(58, 58), this.Tooltiptext[3]);
		this.Savebt = createButton("/resources/save48.png", new Dimension(58, 58), this.Tooltiptext[4]);
		this.LoadBt = createButton("/resources/wczytaj48.png", new Dimension(58, 58), this.Tooltiptext[5]);
		this.Helpbt = createButton("/resources/help48.png", new Dimension(58, 58), this.Tooltiptext[6]);
		this.ButtonPanel.add(this.newNetworkBt);
		this.ButtonPanel.add(this.LoadDataBt);
		this.ButtonPanel.add(this.TrainBt);
		this.ButtonPanel.add(this.MachtBt);
		this.ButtonPanel.add(this.Savebt);
		this.ButtonPanel.add(this.LoadBt);
		this.ButtonPanel.add(this.Helpbt);
		this.contentpane.add(this.ButtonPanel,"West");
	}
	

	
	public JMenuItem CreateJMenuItem(String name, KeyStroke keystroke, JMenu menu,String Pathicon){
		JMenuItem menuItem = new JMenuItem(name);
		menuItem.setIcon(new ImageIcon(MainWindow.class.getResource(Pathicon)));
		menuItem.setAccelerator(keystroke);
		menuItem.addActionListener(this);
		menu.add(menuItem);
		return menuItem;
	}
	public JMenu CreateMenu(String mName, int keystroke,JMenuBar menubar){
		JMenu Menu = new JMenu(mName);
		Menu.setMnemonic(keystroke);
		menubar.add(Menu);
		return Menu;
	}
	public Container SetMaxSize(Dimension Size ,Container object){
		
		object.setMaximumSize(Size);
		object.setMaximumSize(Size);
		object.setPreferredSize(Size);
		return object;
	}
	public JButton createButton(String Pathicon, Dimension Size,  String toltiptext){
		JButton button = new JButton(new ImageIcon(MainWindow.class.getResource(Pathicon)));
		button.setPreferredSize(Size);
		button.setMaximumSize(Size);
		button.setMinimumSize(Size);
		button.addActionListener(this);
		button.setToolTipText(toltiptext);
		return button;
	}
	public void CloseWindow()
	{
		int a = JOptionPane.showConfirmDialog(this, "Czy zako�czy� program ?",
				"Ko�czenie pracy programu",JOptionPane.OK_CANCEL_OPTION );
	if (a == JOptionPane.OK_OPTION )
		{
		
			System.exit(0);
		}
		else
			{
				return;
			}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		
		if(s == this.FNewNet || s == this.newNetworkBt){
			
			this.newnetworkDialog.SetVisiableDialog();
			if(this.newnetworkDialog.isCreate()){
			this.NetworkSet.add(this.numofNet, new TrainNetwork(this.newnetworkDialog.getNetworkInputs(), this.newnetworkDialog.getNetworkOutputs(), this.newnetworkDialog.getNetworkLearningRate()));
			this.NetworkSet.get(this.numofNet).setIDNet(this.numofNet);
			this.internalFrames.add(this.numofNet, new InternalWindow(this.numofNet , this,this.deskpane.getSize() ));
			this.internalFrames.get(this.numofNet).setName(Integer.toString(this.numofNet));
			this.internalFrames.get(this.numofNet).show();			
			this.deskpane.add(this.internalFrames.get(this.numofNet));
			this.numofcurent=this.numofNet;
			this.numofNet++;
			}
		}
		else if (s == this.FClose){
			this.CloseWindow();
		}
		else if ((s == this.FLoadData ) || (s == this.LoadDataBt)){
			this.filechooser.setDialogTitle("Wybierz plik z danymi trenuj�cymi");
			int ret = this.filechooser.showOpenDialog(this);
			if (ret == JFileChooser.APPROVE_OPTION){
				
				this.NetworkSet.get(this.numofcurent).LoadAndNormData(this.filechooser.getSelectedFile().getAbsolutePath());
				}
			this.filechoosername.setSelectedFile(this.filechooser.getSelectedFile());
			this.filechoosername.setDialogTitle("Wybierz plik z nazwami");
				int ret2 = this.filechoosername.showOpenDialog(this);
				if(ret2 == JFileChooser.APPROVE_OPTION){
					this.loadnames.ReadNames(this.filechoosername.getSelectedFile().getAbsolutePath(),
							this.newnetworkDialog.getNetworkInputs()+this.newnetworkDialog.getNetworkOutputs());
				
				this.internalFrames.get(this.numofcurent).CreatePanelslider(
						this.NetworkSet.get(this.numofcurent).getMaxValue(), 
						this.NetworkSet.get(this.numofcurent).getMinValue(),
						this.NetworkSet.get(this.numofcurent).getNetworkInput(),
						this.loadnames.getNames());
				
				this.internalFrames.get(this.numofcurent).CreateOutputPanel(
						this.NetworkSet.get(this.numofcurent).getNetworkInput(),
						this.NetworkSet.get(this.numofcurent).getNetworkOutput(), 
						this.loadnames.getNames(),true);
				
				this.internalFrames.get(this.numofcurent).CreateStatusPanel(
						this.newnetworkDialog.getNetworkLearningRate(),
						this.NetworkSet.get(this.numofcurent).getNetworkInput(),
						this.NetworkSet.get(this.numofcurent).getNetworkHiddenNeuron(),
						this.NetworkSet.get(this.numofcurent).getNetworkOutput());
				invalidate();
				repaint();
				}
			}
					
		
		else if (s == this.OTrain || s == this.TrainBt){
			this.internalFrames.get(this.numofcurent).SetError((Double.toString(this.NetworkSet.get(this.numofcurent).TrainingNetwork())));
		}
		else if(s == this.MachtBt || s == this.OMacht){
			double [] recognizeoutputs = this.NetworkSet.get(this.numofcurent)
					.Recognize(this.internalFrames.get(this.numofcurent)
							.GetInputslider((this.NetworkSet.get(this.numofcurent).getNetworkInput()))); 
			
			this.internalFrames.get(this.numofcurent).SetOutputsLabel(this.NetworkSet.get(this.numofcurent).getNetworkOutput()
					, recognizeoutputs);
		}
		else if (s == this.SSave || s == this.Savebt){
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Wybierz miejsce zapisu struktury sieci");
			int ret3=chooser.showSaveDialog(this);
			if(ret3 == JFileChooser.APPROVE_OPTION){
				 		        
			            File file = chooser.getSelectedFile();

			            String fname = file.getAbsolutePath();

			            if(!fname.endsWith(".xml") ) {
			                file = new File(fname + ".xml");
			                fname =file.getAbsolutePath();
			                }	              
			            
			new XmlNetworkStructWriter().write(fname, this.internalFrames.get(this.numofcurent).getLabelnames(),
					this.internalFrames.get(this.numofcurent).getOutputNames(), 
					this.NetworkSet.get(this.numofcurent).getMinValue(), 
					this.NetworkSet.get(this.numofcurent).getMaxValue(), 
					this.NetworkSet.get(this.numofcurent).getInputHiddenWeight(),
					this.NetworkSet.get(this.numofcurent).getHiddenOutputWeight(),
					this.NetworkSet.get(this.numofcurent).getNetworkInput(),
					this.NetworkSet.get(this.numofcurent).getNetworkOutput(),
					this.NetworkSet.get(this.numofcurent).getNetworkHiddenNeuron(),
					this.NetworkSet.get(this.numofcurent).getErrorNetwork());
			this.internalFrames.get(this.numofcurent).setTitle(file.getName());
			}
		}
		else if (s == this.SRead || s== this.LoadBt){
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogTitle("Wybierz plik zawieraj�cy struktur� sieci");
			int ret3=chooser.showOpenDialog(this);
			if(ret3 == JFileChooser.APPROVE_OPTION){
				XmlNetworkStructReader Networkreader = new XmlNetworkStructReader();
				Networkreader.read(chooser.getSelectedFile().getAbsolutePath(),
						this.NetworkSet.get(this.numofcurent).getNetworkInput(),
						this.NetworkSet.get(this.numofcurent).getNetworkOutput(),
						this.NetworkSet.get(this.numofcurent).getNetworkHiddenNeuron());
				this.internalFrames.get(this.numofcurent).setTitle(chooser.getSelectedFile().getName());
				this.internalFrames.get(this.numofcurent).CreatePanelslider(
						Networkreader.getMaxData(), 
						Networkreader.getMinData(),
						this.NetworkSet.get(this.numofcurent).getNetworkInput(),
						Networkreader.getInputNames());
				
				this.internalFrames.get(this.numofcurent).CreateOutputPanel(
						this.NetworkSet.get(this.numofcurent).getNetworkInput(),
						this.NetworkSet.get(this.numofcurent).getNetworkOutput(), 
						Networkreader.getOutputsName(),false);
				
				this.internalFrames.get(this.numofcurent).CreateStatusPanel(
						this.newnetworkDialog.getNetworkLearningRate(),
						this.NetworkSet.get(this.numofcurent).getNetworkInput(),
						this.NetworkSet.get(this.numofcurent).getNetworkHiddenNeuron(),
						this.NetworkSet.get(this.numofcurent).getNetworkOutput());
				this.internalFrames.get(this.numofcurent).SetError(Double.toString(Networkreader.getNetError()));
				this.NetworkSet.get(this.numofcurent).SetNetworkWeight(Networkreader.getInputHiddenWeight(), Networkreader.getHiddenOutputWeight());
				this.NetworkSet.get(this.numofcurent).SetNetworkmaxMin(Networkreader.getMaxData(), Networkreader.getMinData());
				invalidate();
				repaint();
				
			}
		}
	}
	
	@Override
	public void internalFrameActivated(InternalFrameEvent arg0) {
		
		this.numofcurent=Integer.parseInt(arg0.getInternalFrame().getName());
		
	}
	@Override
	public void internalFrameClosed(InternalFrameEvent arg0) {
		
		
	}
	@Override
	public void internalFrameClosing(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void internalFrameDeactivated(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void internalFrameDeiconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void internalFrameIconified(InternalFrameEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void internalFrameOpened(InternalFrameEvent arg0) {
		this.numofcurent=Integer.parseInt(arg0.getInternalFrame().getName());
		
		
		
	}
	
	
}
