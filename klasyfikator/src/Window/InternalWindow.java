package Window;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameListener;

import Dialogs.NewNetworkDialog;



public class InternalWindow extends JInternalFrame implements  ChangeListener {

	private JPanel sliderPanel;
	private JPanel outputPanel;
	private JPanel statusBarPanel;
	private JPanel MainPanel;
	private double MaxDataValue[];
	private double MinDataValue[];
	private ArrayList<JSlider> slider;
	private ArrayList<JLabel> LabelSlider;
	private ArrayList<JLabel> NameList;
	private ArrayList<JLabel> NameOutputList;
	private ArrayList<JLabel> OutputList;
	private JLabel	StBarNameNI;
	private JLabel  StBarNI;
	private JLabel  StBarNameNH;
	private JLabel  StBarNH;
	private JLabel  StBarNameNO;
	private JLabel  StBarNO;
	private JLabel  StBarNameER;
	private JLabel  StBarNeR;
	private JLabel  StBarNameLR;
	private JLabel 	StBarLR;
	private String[] labelnames;
	private String[] OutputNames;

	private int inputNumber;

	private int IdFrame;
	public InternalWindow(int index, InternalFrameListener l,Dimension size){
		super("Sie� neuronowa "+ index, true, true, true, true);
		setLayout(null);
		setSize(size);
		setResizable(false);
		addInternalFrameListener(l);
		setFrameIcon(new ImageIcon(InternalWindow.class.getResource("/resources/newfile24.png")));
		setBorder(BorderFactory.createRaisedSoftBevelBorder());
		moveToFront();
		
		this.slider = new ArrayList<JSlider>();
		this.LabelSlider = new ArrayList<JLabel>();
		this.NameList = new ArrayList<JLabel>();
		this.NameOutputList = new ArrayList<JLabel>();
		this.OutputList = new ArrayList<JLabel>();
		this.sliderPanel = new JPanel();
		this.outputPanel = new JPanel();
		this.statusBarPanel = new JPanel();
		this.IdFrame=0;
		this.inputNumber=0;
		this.StBarNeR = new JLabel();
		
	}
	public void CreatePanelslider(double maxdata[], double []mindata, int inputnumber,String [] names){
		this.labelnames = names;
		this.inputNumber= inputnumber;
		this.sliderPanel.setLayout(null);
		TitledBorder title;
		title = BorderFactory.createTitledBorder(
		                       BorderFactory.createLineBorder(Color.BLACK), "Dane Wej�ciowe");
		title.setTitleJustification(TitledBorder.CENTER);
		this.sliderPanel.setBorder(title);
		this.setAutoscrolls(true);
		
		
		
		int y=15;
		for(int i = 0; i< inputnumber;i++){
			
			this.NameList.add(i,new JLabel());
			this.NameList.get(i).setText(labelnames[i]);
			this.NameList.get(i).setHorizontalAlignment(JLabel.RIGHT);
			this.NameList.get(i).setBounds(10, y, 150, 25);
									
			this.slider.add(i, new JSlider((int)(mindata[i]*100),(int)(maxdata[i]*100),(int)(mindata[i]*100)));
			this.slider.get(i).addChangeListener(this);		
			this.slider.get(i).setBounds(170, y, 300, 25);
			
			this.LabelSlider.add(i,new JLabel());			
			this.LabelSlider.get(i).setBounds(480, y, 50, 25);
			this.LabelSlider.get(i).setText(Double.toString(mindata[i]));
			
			this.sliderPanel.add(this.NameList.get(i));
			this.sliderPanel.add(this.slider.get(i));
			this.sliderPanel.add(this.LabelSlider.get(i));
			y+=40;
			
		}
		this.sliderPanel.setBounds(5, 5, 550, 535);
		add(this.sliderPanel);
	}
	
	
	public void CreateOutputPanel(int inputn, int outputn, String[] names,boolean lang){
		
		String [] namesA = names;
		 
		TitledBorder title;
		title = BorderFactory.createTitledBorder(
		                       BorderFactory.createLineBorder(Color.BLACK), "Wyniki Rozpoznania");
		title.setTitleJustification(TitledBorder.CENTER);
		
		this.outputPanel.setLayout(null);
		this.outputPanel.setBorder(title);
		if(lang){
			this.OutputNames = new String [outputn];
			for(int i = 0; i < inputn+outputn;i++){
			if (i<inputn){
				
			}
			else{
				OutputNames[i-inputn] = namesA[i];
			}
		
			}
		}else{
			
			OutputNames= names;
		}
		
		int y =55;
		for (int j= 0; j < outputn;j++){
			
			this.NameOutputList.add(j,new JLabel());
			this.NameOutputList.get(j).setBounds(10,y,150,25);
			this.NameOutputList.get(j).setText(OutputNames[j]);
			this.NameOutputList.get(j).setHorizontalAlignment(JLabel.RIGHT);
			this.OutputList.add(j,new JLabel());
			this.OutputList.get(j).setBounds(180, y, 100, 25);
			this.OutputList.get(j).setText("0.0");
			this.outputPanel.add(this.NameOutputList.get(j));
			this.outputPanel.add(this.OutputList.get(j));
			y+=40;
		}
		
		this.outputPanel.setBounds(560, 5, 355, 535);
		add(this.outputPanel);
	}
	public void CreateStatusPanel( double LerningRate,int InputsNum, int HiddenNeuronsNum, int OutputsNum){
		TitledBorder title;
		title = BorderFactory.createTitledBorder(
		                       BorderFactory.createLineBorder(Color.BLACK), "Parametry Sieci");
		title.setTitleJustification(TitledBorder.CENTER);
		this.statusBarPanel.setBorder(title);
		this.statusBarPanel.setLayout(null);
		this.StBarNameNI = new JLabel("Liczba neuron�w wej�ciowych: ");
		this.StBarNameNI.setHorizontalAlignment(JLabel.RIGHT);
		this.StBarNameNI.setBounds(10, 15, 150, 25);
		this.StBarNI = new JLabel(Integer.toString(InputsNum));
		this.StBarNI.setBounds(165, 15, 30, 25);
		this.StBarNameNH = new JLabel("Liczba neuron�w ukrytych: ");
		this.StBarNameNH.setBounds(200, 15, 150, 25);
		this.StBarNameNH.setHorizontalAlignment(JLabel.RIGHT);
		this.StBarNH = new JLabel(Integer.toString(HiddenNeuronsNum));
		this.StBarNH.setBounds(355, 15, 30, 25);
		this.StBarNameNO = new JLabel("Liczba neuron�w wyj�ciowych: ");
		this.StBarNameNO.setBounds(400, 15, 150, 25);
		this.StBarNameNO.setHorizontalAlignment(JLabel.RIGHT);
		this.StBarNO = new JLabel(Integer.toString(OutputsNum));
		this.StBarNO.setBounds(555,15,30,25);
		this.StBarNameLR = new JLabel("Wsp. uczenia sieci: ");
		this.StBarNameLR.setBounds(590, 15, 120, 25);
		this.StBarNameLR.setHorizontalAlignment(JLabel.RIGHT);
		this.StBarLR = new JLabel(Double.toString(LerningRate));
		this.StBarLR.setBounds(615, 15, 30, 25);
		this.StBarNameER = new JLabel("B��d sieci: ");
		this.StBarNameER.setHorizontalAlignment(JLabel.RIGHT);
		this.StBarNameER.setBounds(650, 15, 80, 25);
		this.StBarNeR.setText("0.0");
		this.StBarNeR.setBounds(735, 15, 140, 25);
		
		this.statusBarPanel.add(this.StBarNameNI);
		this.statusBarPanel.add(this.StBarNI);
		this.statusBarPanel.add(this.StBarNameNH);
		this.statusBarPanel.add(this.StBarNH);
		this.statusBarPanel.add(this.StBarNameNO);
		this.statusBarPanel.add(this.StBarNO);
		this.statusBarPanel.add(this.StBarNameER);
		this.statusBarPanel.add(this.StBarNeR);
		
		
		this.statusBarPanel.setBounds(5, 540, 910, 55);
		add(this.statusBarPanel);
	}
	public void SetError(String text){
		this.StBarNeR.setText(text);
		this.StBarNeR.setBackground(new Color(252,32,27));
		this.StBarNeR.setOpaque(true);
		
	}
		
	public void SetOutputsLabel(int Numofout , double [] outputs){
		double [] OutputsV = outputs;
		double Max=OutputsV[0];
		int		indexmax=0;
		for(int j=1;j<Numofout;j++){
			if(Max<OutputsV[j]){
				Max=OutputsV[j];
				indexmax = j;
			}
		}
		
		double [] round = new double[Numofout];
		for (int i=0; i < Numofout; i++){
	        round[i]= round(OutputsV[i], 7);
			this.OutputList.get(i).setText(Double.toString(round[i]));
			this.OutputList.get(i).setOpaque(false);
		}
		this.OutputList.get(indexmax).setBackground(new Color(252,32,27));
		this.OutputList.get(indexmax).setOpaque(true);
	}
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	public double[] GetInputslider(int numofinput){
		double[] inputslider = new double[numofinput];
		for(int i=0; i< numofinput; i++){
			inputslider[i]=(double)this.slider.get(i).getValue()/100;
		}
		return inputslider;
	}
	public String[] getLabelnames() {
		return labelnames;
	}
	public void setLabelnames(String[] labelnames) {
		this.labelnames = labelnames;
	}
	public String[] getOutputNames() {
		return OutputNames;
	}
	public void setOutputNames(String[] outputNames) {
		OutputNames = outputNames;
	}
	public int getIdFrame() {
		return IdFrame;
	}
	public void setIdFrame(int idFrame) {
		IdFrame = idFrame;
	}
	public double[] getMaxDataValue() {
		return MaxDataValue;
	}
	public void setMaxDataValue(double[] maxDataValue) {
		MaxDataValue = maxDataValue;
	}
	public double[] getMinDataValue() {
		return MinDataValue;
	}
	public void setMinDataValue(double[] minDataValue) {
		MinDataValue = minDataValue;
	}
	@Override
	public void stateChanged(ChangeEvent e) {
		for(int i = 0;i<this.inputNumber;i++){
			this.LabelSlider.get(i).setText(String.valueOf((double)this.slider.get(i).getValue()/100));
		}
		
	}
}
