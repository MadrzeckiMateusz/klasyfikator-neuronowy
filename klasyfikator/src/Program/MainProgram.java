package Program;

import java.awt.EventQueue;

import Window.MainWindow;

public class MainProgram {
	public static void main(String[] args){
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{						
					new MainWindow(true);
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
}
