package Data;



public class RandomUnique {

	 // tablica bit�w przechowuj�ca liczby
    private boolean[] liczby;

    // aktulna wielko�� tablicy
    private int size;

    // konstruktor przyjmuje jako argument wielko�� zbioru
    public RandomUnique(int size) {
            // tworzymy tablic� bit�w
            liczby = new boolean[size];
            this.size = size;
            // Wype�niamy j� danymi
            
            
    }
    public void init(){
    	for (int i = 0; i < size; i++) {
            liczby[i] = true;
    }
    }
    public int get() {
            // losujemy liczb�
            int i = (int) (Math.random() * this.size) + 1;

            // sprawdzamy czy tak� liczb� ju� wylosowano je�eli na pozycji i-tej
            // znajduje si� true to znaczy, �e liczba nie by�a wylosowana
            if (liczby[i - 1] == true) {
                    // oznaczmy liczb� jako wylosowan�
                    liczby[i - 1] = false;
                    return i; // zwracamy liczb�
            } else {
                    // liczba si� powt�rzy�a. Ponawiamy losowanie
                    return get();
            }
    }
}
 
