package Data;


	 
	 
	import java.io.File;
	 
	import javax.xml.parsers.DocumentBuilder;
	import javax.xml.parsers.DocumentBuilderFactory;
	import javax.xml.transform.OutputKeys;
	import javax.xml.transform.Transformer;
	import javax.xml.transform.TransformerFactory;
	import javax.xml.transform.dom.DOMSource;
	import javax.xml.transform.stream.StreamResult;
	 
	import org.w3c.dom.Document;
	import org.w3c.dom.Element;
	import org.w3c.dom.Node;
	 
	 
	public class XmlNetworkStructWriter  {
		
	 
	    public void write(String path,String [] InputsName,String [] OutputsName, 
	    		double [] MinData, double [] MaxData, double[][] InputHiddenWeight,
	    		double[][] HiddenOutputWeight,int NumInputs, int NumOutputs, 
	    		int NumHidden,double Error) {
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder;
	        try {
	            dBuilder = dbFactory.newDocumentBuilder();
	            Document doc = dBuilder.newDocument();
	            //add elements to Document
	            Element rootElement =
	                doc.createElement("NetworkSet");
	            //append root element to document
	            doc.appendChild(rootElement);
	 
	            
	            rootElement.appendChild(getInputNames(doc, InputsName, NumInputs));
	            rootElement.appendChild(getOutputsNames(doc, OutputsName, NumOutputs));
	            rootElement.appendChild(getMinValue(doc, MinData, NumInputs));
	            rootElement.appendChild(getMaxValue(doc, MaxData, NumInputs));
	            rootElement.appendChild(getInputHiddenWeights(doc, InputHiddenWeight, NumInputs, NumHidden));
	            rootElement.appendChild(getHiddenOutputWeights(doc, HiddenOutputWeight, NumHidden, NumOutputs));
	            rootElement.appendChild(getError(doc, Error, NumOutputs));
	            
	            
	 
	            //for output to file, console
	            TransformerFactory transformerFactory = TransformerFactory.newInstance();
	            Transformer transformer = transformerFactory.newTransformer();
	            //for pretty print
	            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	            DOMSource source = new DOMSource(doc);
	 
	            //write to console or file
	            
	            StreamResult file = new StreamResult(new File(path));
	 
	            //write data
	        
	            transformer.transform(source, file);
	           
	 
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	 
	 
	   
	    private  Node getInputNames(Document doc, String [] Inputnames, int NumberInputs ) {
	        Element InputNames = doc.createElement("Input_Names");
	 
	        for(int i=0;i<NumberInputs;i++){
	        	String name = "I_Names_"+i;
	  	       InputNames.appendChild(getElements(doc, InputNames, name,Inputnames[i] ));
	        }
	        return InputNames;
	    }
	    private  Node getError(Document doc, Double Error, int NumberOutputs ) {
	        Element NetworkError = doc.createElement("Network_Error");
	 
	        
	        NetworkError.appendChild(getElements(doc, NetworkError, "Error",Double.toString(Error) ));
	        
	        return NetworkError;
	    }
	    private  Node getOutputsNames(Document doc, String [] Outputnames, int NumberOutputs ) {
	        Element OutputNames = doc.createElement("Output_Names");
	 
	        for(int i=0;i<NumberOutputs;i++){
	        	String name = "O_Names_"+i;
	        	OutputNames.appendChild(getElements(doc, OutputNames, name,Outputnames[i] ));
	        }
	        return OutputNames;
	    }
	     
	    private  Node getMinValue(Document doc, double [] minValues, int NumberInputs ) {
	        Element MinValue = doc.createElement("MinValue");
	 
	        for(int i=0;i<NumberInputs;i++){
	        	String name = "Min_"+i;
	        	MinValue.appendChild(getElements(doc, MinValue, name,Double.toString(minValues[i]) ));
	        }
	        return MinValue;
	    }
	    private  Node getMaxValue(Document doc, double [] maxValues, int NumberInputs ) {
	        Element MaxValue = doc.createElement("MaxValue");
	 
	        for(int i=0;i<NumberInputs;i++){	
	        	String name = "Max_"+i;
	        	MaxValue.appendChild(getElements(doc, MaxValue, name,Double.toString(maxValues[i]) ));
	        }
	        return MaxValue;
	    }
	    private  Node getInputHiddenWeights(Document doc, double [][] InHidWeights , int NumberInputs, int NumberHidden ) {
	        Element WeInHid = doc.createElement("Weight_Input_Hidden");
	 
	        for(int i=0;i<NumberInputs+1;i++){
	        	
	        	for( int j=0;j<NumberHidden; j++){
	        		String name = "Row_"+i+"_"+j;
		        	WeInHid.appendChild(getElements(doc, WeInHid,name ,Double.toString(InHidWeights[i][j]) ));
	        	}
	        }
	        return WeInHid;
	    }
	    private  Node getHiddenOutputWeights(Document doc, double [][] HiddOutWeigh ,  int NumberHidden, int NumberOutputs ) {
	        Element WeHiddOut = doc.createElement("Weight_Hidden_Output");
	 
	        for(int i=0;i<NumberHidden+1;i++){
	        	
	        	for( int j=0;j<NumberOutputs; j++){
	        		String name = "Row_"+i+"_"+j;
		        	WeHiddOut.appendChild(getElements(doc, WeHiddOut,name ,Double.toString(HiddOutWeigh[i][j]) ));
	        	}
	        }
	        return WeHiddOut;
	    }
	     
	 
	 
	    //utility method to create text node
	    private  Node getElements(Document doc, Element element, String name, String value) {
	        Element node = doc.createElement(name);
	        node.appendChild(doc.createTextNode(value));
	        return node;
	    }
	}
	                
	  

