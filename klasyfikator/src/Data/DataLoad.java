package Data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class DataLoad {
	
	private File file;
	private FileReader fr;
	private Scanner scanerIn;
	private double [][] Dane;
	private int Lwierszy;
	private int Lkolumn;
	private String linia;
    final Pattern SEP = Pattern.compile(",");
	
	
	public DataLoad(){
		
		this.Lwierszy = 0;
		this.Lkolumn = 0;
		this.Dane = null;
		
	}
	
	public void ReadData(String nazwa){
		
		try {
			this.fr = new FileReader(nazwa);
			
	      } catch (FileNotFoundException e) {
	         
	          System.exit(0);
	      }
		 String li;
		 BufferedReader bfr = new BufferedReader(fr);
			try {
				while((li = bfr.readLine()) != null){
				    this.Lwierszy +=1;
				    String tab[] = SEP.split(li);
				    this.Lkolumn = tab.length;
				 }
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		this.file = new File(nazwa);
		this.Dane = new double[this.Lwierszy][this.Lkolumn];
		try {
			this.scanerIn = new Scanner(this.file);
			for(int i=0;i<=this.Lwierszy-1;i++){
				this.linia = this.scanerIn.nextLine();
			String[] tab = SEP.split(this.linia);
			
				for(int j=0;j<=this.Lkolumn-1;j++){
					this.Dane[i][j]= Double.valueOf(tab[j]);
				}
		
		} 
		}catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
	}

	public double[][] getDane() {
		return Dane;
	}

	public void setDane(double[][] dane) {
		Dane = dane;
	}

	public int getLwierszy() {
		return Lwierszy;
	}

	public void setLwierszy(int lwierszy) {
		Lwierszy = lwierszy;
	}

	public int getLkolumn() {
		return Lkolumn;
	}

	public void setLkolumn(int lkolumn) {
		Lkolumn = lkolumn;
	}
	
	
	
	
}