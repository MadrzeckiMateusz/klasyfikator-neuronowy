package Data;

public class DataNorm {
	
	
	
	private int Lkolumn;
	private int Lwierszy;
	private int LVectorTraining;
	private int LCheckVector;
	
	private int[] NumOfClass;
	private int[] NumOfCheckVector;
	private int[] NumOfTrainingVector;
	
	private double [] MaxValue;
	private double [] MinValue;
	
	private double [][] DaneNorm;
	private double [][] DaneTrain;
	private double [][] DataCheck;
	public DataNorm(){
		
	}
	public DataNorm(int lkolumn, int lwierszy){
		
		this.Lkolumn = lkolumn;
		this.Lwierszy = lwierszy;
		this.LVectorTraining =0;
		this.LCheckVector = 0;
		this.NumOfClass = null;
		this.NumOfCheckVector = null;
		this.NumOfTrainingVector =null;
		
		
		this.MaxValue = new double[this.Lkolumn];
		this.MinValue = new double[this.Lkolumn];
		
		this.DaneNorm = new double[this.Lwierszy][this.Lkolumn];
		this.DaneTrain= null;
		this.DataCheck= null;
	}
	
	public void NormalizeData(double [][] DaneTmp,int loutputs){
		this.NumOfClass = new int[loutputs];
		this.NumOfCheckVector = new int[loutputs];
		this.NumOfTrainingVector = new int[loutputs];
	/*if(EndBegin == true){// je�li wektor trenujacy jest na pocz�tku
	
			
			//Maksimum w zbiorze danych 
			for(int i=1;i<=this.Lkolumn-1;i++){
				this.MaxValue[i-1]=DaneTmp[0][i];
				for(int j=0;j<=this.Lwierszy-1;j++){
					if(DaneTmp[j][i]>this.MaxValue[i-1])
						this.MaxValue[i-1]=DaneTmp[j][i];
				}
			}
			//Minimum w zbiorze danych
			for(int i=1;i<=this.Lkolumn-1;i++){
				this.MinValue[i-1]=DaneTmp[0][i];
				for(int j=0;j<=this.Lwierszy-1;j++){
					if(DaneTmp[j][i]<this.MinValue[i-1])
						this.MinValue[i-1]=DaneTmp[j][i];
				}
			}
			//Normalizacja danych do warto�ci z przedzia�u <0,1>
			for(int i=0;i<=this.Lkolumn-1;i++){
				if (i == 0){
					for(int j=0;j<=this.Lwierszy-1;j++){
					 this.DaneNorm[j][i]=DaneTmp[j][i];
						this.NumOfClass[(int)DaneTmp[j][i]-1] += 1;
		
					}
				}
				else{
					for(int j=0;j<=this.Lwierszy-1;j++){
						this.DaneNorm[j][i]=((DaneTmp[j][i]-this.MinValue[i-1])/
								(this.MaxValue[i-1]-this.MinValue[i-1]));
						this.DaneNorm[j][i]*=10000;
						this.DaneNorm[j][i] = Math.round(this.DaneNorm[j][i]);
						this.DaneNorm[j][i]/= 10000;
					}
				}
			}
		}
		else // dla odpowiedzi znajdujacej sie na ko�cu
		{*/
			for (int i = 0; i < this.Lkolumn - 1; i++)
		    {
		      this.MaxValue[i] = DaneTmp[0][i];
		      for (int j = 0; j <= this.Lwierszy - 1; j++) {
		        if (DaneTmp[j][i] > this.MaxValue[i]) {
		          this.MaxValue[i] = DaneTmp[j][i];
		        }
		      }
		    }
		    for (int i = 0; i < this.Lkolumn - 1; i++)
		    {
		      this.MinValue[i] = DaneTmp[0][i];
		      for (int j = 0; j <= this.Lwierszy - 1; j++) {
		        if (DaneTmp[j][i] < this.MinValue[i]) {
		          this.MinValue[i] = DaneTmp[j][i];
		        }
		      }
		    }
		    for (int i = 0; i <= this.Lkolumn - 1; i++) {
		      if (i == this.Lkolumn - 1) {
		        for (int j = 0; j <= this.Lwierszy - 1; j++) {
		          this.DaneNorm[j][i] = DaneTmp[j][i];
		          this.NumOfClass[(int)DaneTmp[j][i]-1] += 1;
		        }
		      } else {
		        for (int j = 0; j <= this.Lwierszy - 1; j++)
		        {
		          this.DaneNorm[j][i] = 
		            ((DaneTmp[j][i] - this.MinValue[i]) / (this.MaxValue[i] - this.MinValue[i]));
		          this.DaneNorm[j][i] *= 10000.0D;
		          this.DaneNorm[j][i] = Math.round(this.DaneNorm[j][i]);
		          this.DaneNorm[j][i] /= 10000.0D;
		        }
		      }
		    }
	//	}
			for(int i=0; i<loutputs; i++){
				this.NumOfCheckVector[i]= (int)(this.NumOfClass[i]*0.25);
				this.LCheckVector += this.NumOfCheckVector[i];
				this.NumOfTrainingVector[i]=(this.NumOfClass[i]- this.NumOfCheckVector[i]);
				this.LVectorTraining +=this.NumOfTrainingVector[i];
			}
			
		}
	public void DataTraining(double [][] dane ,int loutputs){
		this.DaneTrain = new double[this.LVectorTraining][this.Lkolumn];
		this.DataCheck = new double[this.LCheckVector][this.Lkolumn];
		int l =0;
		int z=0;
		int l2=0;
		int l1 = 0;
		int licznik= this.NumOfClass[z];
		int licznik2 = this.NumOfTrainingVector[z];
		for(int i=0;i<loutputs;i++){
			
			while(l<licznik){
				if(l < licznik2){
					for (int j=0;j<this.Lkolumn;j++){
						this.DaneTrain[l1][j]=dane[l][j];
					}
					l1++;
				}
				else{
					for (int j=0;j<this.Lkolumn;j++){
						this.DataCheck[l2][j]=dane[l][j];
					}
					l2++;
				}
			l++;
			}
			z++;
			licznik+=this.NumOfClass[z];
			licznik2=l+this.NumOfTrainingVector[z];
		}
		
		
	}
	public double[] NormalizeVector(double[] vector,int linputs){
		
		for(int i=0;i<=linputs-1;i++){
			vector[i]=((vector[i]-this.MinValue[i])/
					(this.MaxValue[i]-this.MinValue[i]));
		}
		return vector;
		
	}
	
	public double[] SetTrainingVector(double [][]dane ,int wiersz,int linputs){
		double DaneZN[][] = dane;
		double[] TrainingVector = new double[linputs];
	/*	if(EndBegin == true){
		for(int i=0; i <linputs; i++ ){
			TrainingVector[i] = DaneZN[wiersz][i+1];
		}
		}
		else{*/
			for (int i = 0; i < linputs; i++) {
			      TrainingVector[i] = DaneZN[wiersz][i];
			    }
		//}
		return TrainingVector;
	}
	public double[] SetCorectAnwer(double [][] dane,int wiersz, int loutputs){
		double [] CorectAnswer = new double[loutputs];
		double [][] DaneZN = dane;
	    double tmpO = 1.0;
	 /*   if (EndBegin == true)
	    {
	    for (int i = 0; i < loutputs; i++)
	    {
	      if (DaneZN[wiersz][0] == tmpO) {
	        CorectAnswer[i] = 1.0;
	      } else {
	        CorectAnswer[i] = 0.0;
	      }
	      tmpO += 1.0;
	    }
	    }
	    else{*/
	    	for (int i = 0; i < loutputs; i++)
	        {
	          if (DaneZN[wiersz][(this.Lkolumn - 1)] == tmpO) {
	            CorectAnswer[i] = 1.0;
	          } else {
	            CorectAnswer[i] = 0.0;
	          }
	          tmpO += 1.0;
	        }
	   // }

			return CorectAnswer;
			}
	
	

	public int getLCheckVector() {
		return LCheckVector;
	}
	public void setLCheckVector(int lCheckVector) {
		LCheckVector = lCheckVector;
	}
	public int getLVectorTraining() {
		return LVectorTraining;
	}
	public void setLVectorTraining(int lVectorTraining) {
		LVectorTraining = lVectorTraining;
	}
	public int[] getNumOfCheckVector() {
		return NumOfCheckVector;
	}
	public void setNumOfCheckVector(int[] numOfCheckVector) {
		NumOfCheckVector = numOfCheckVector;
	}
	public int[] getNumOfTrainingVector() {
		return NumOfTrainingVector;
	}
	public void setNumOfTrainingVector(int[] numOfTrainingVector) {
		NumOfTrainingVector = numOfTrainingVector;
	}
	public double[][] getDaneTrain() {
		return DaneTrain;
	}
	public void setDaneTrain(double[][] daneTrain) {
		DaneTrain = daneTrain;
	}
	public double[][] getDataCheck() {
		return DataCheck;
	}
	public void setDataCheck(double[][] dataCheck) {
		DataCheck = dataCheck;
	}
	public int[] getNumOfClass() {
		return NumOfClass;
	}
	public void setNumOfClass(int[] numOfClass) {
		NumOfClass = numOfClass;
	}
	public int getLkolumn() {
		return Lkolumn;
	}

	public void setLkolumn(int lkolumn) {
		Lkolumn = lkolumn;
	}

	public int getLwierszy() {
		return Lwierszy;
	}

	public void setLwierszy(int lwierszy) {
		Lwierszy = lwierszy;
	}

	public double[] getMaxValue() {
		return MaxValue;
	}

	public void setMaxValue(double[] maxValue) {
		MaxValue = maxValue;
	}

	public double[] getMinValue() {
		return MinValue;
	}

	public void setMinValue(double[] minValue) {
		MinValue = minValue;
	}

	public double[][] getDaneNorm() {
		return DaneNorm;
	}

	public void setDaneNorm(double[][] daneNorm) {
		DaneNorm = daneNorm;
	}
		
	
	}
