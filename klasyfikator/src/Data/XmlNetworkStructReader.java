package Data;


	import java.io.File;
import java.io.IOException;

	import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
	 

	import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
	 
	 
	public class XmlNetworkStructReader {
	 private String[] InputNames;
	 private String[] OutputsName;
	 private double[] MinData;
	 private double[] MaxData;
	 private double[][] InputHiddenWeight;
	 private double[][] HiddenOutputWeight;
	 private double NetError;
	    public void read(String path, int NumInputs , int NumOutpust, int NumHidden){
	        String filePath = path;
	        File xmlFile = new File(filePath);
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder;
	        try {
	            dBuilder = dbFactory.newDocumentBuilder();
	            Document doc = dBuilder.parse(xmlFile);
	            doc.getDocumentElement().normalize();
	            
	            
	            getInputsNames(doc, NumInputs);
	           this.OutputsName= getOutputsNames(doc, NumOutpust);
	           this.MinData = getMinMaxValue(doc, NumInputs, "MinValue", "Min_");
	           this.MaxData = getMinMaxValue(doc, NumInputs, "MaxValue", "Max_");
	            getInputHidenWeight(doc, NumInputs, NumHidden);
	            getHiddenOutputWeight(doc, NumHidden, NumOutpust);	
	            getError(doc);
	           
	            //lets print Employee list information
	            
	        } catch (SAXException | ParserConfigurationException | IOException e1) {
	            e1.printStackTrace();
	        }
	 
	    }
	 
	    private void getError(Document doc){
		   
		   NodeList nodeInput_name = doc.getElementsByTagName("Network_Error");
	        Node node = nodeInput_name.item(0);
	       
	       
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	           
	          
	                this.NetError=Double.parseDouble((getTagValue("Error", element)));
	                
	           }
	        }
	   
	   private void getInputsNames(Document doc,int numinputs) {
	        	        
	        NodeList nodeInput_name = doc.getElementsByTagName("Input_Names");
	        Node node = nodeInput_name.item(0);
	      
	        this.InputNames = new String[numinputs];
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	           
	           for (int i = 0; i < numinputs; i++) {
	                this.InputNames[i]=(getTagValue("I_Names_"+i, element));
	              
	           }
	        }
	   }
	   private String[] getOutputsNames(Document doc,int Numoutputs) {
	        
	        NodeList nodeInput_name = doc.getElementsByTagName("Output_Names");
	        Node node = nodeInput_name.item(0);
	        
	       String[] Names = new String[Numoutputs];
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	           
	           for (int i = 0; i < Numoutputs; i++) {
	                Names[i]=(getTagValue("O_Names_"+i, element));
	                
	           }
	        }
	        return Names;
	   }
	   private double[] getMinMaxValue(Document doc,int NumInOut,String MainTag, String SecondTag) {
	        
	        NodeList nodeInput_name = doc.getElementsByTagName(MainTag);
	        Node node = nodeInput_name.item(0);
	        
	       double[] TempArray = new double[NumInOut];
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	           
	           for (int i = 0; i < NumInOut; i++) {
	                TempArray[i]=Double.parseDouble(getTagValue(SecondTag+i, element));
	               
	           }
	          
	        }
	        return TempArray;
	   }
	   private void getInputHidenWeight(Document doc,int NumInput,int NumHidden) {
	        
	        NodeList nodeInput_name = doc.getElementsByTagName("Weight_Input_Hidden");
	        Node node = nodeInput_name.item(0);
	        
	       this.InputHiddenWeight = new double[NumInput+1][NumHidden];
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	           
	           for (int i = 0; i < NumInput +1; i++) {
	        	   for(int j=0; j < NumHidden; j++){
	                this.InputHiddenWeight[i][j]=Double.parseDouble(getTagValue("Row_"+i+"_"+j, element));
	               
	        	   }
	        	   
	           }
	           
	        }
	   }
	   private void getHiddenOutputWeight(Document doc,int NumHidden,int NumOutput) {
	        
	        NodeList nodeInput_name = doc.getElementsByTagName("Weight_Hidden_Output");
	        Node node = nodeInput_name.item(0);
	        
	      HiddenOutputWeight = new double[NumHidden+1][NumOutput];
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	           
	           for (int i = 0; i < NumHidden +1; i++) {
	        	   for(int j=0; j < NumOutput; j++){
	                this.HiddenOutputWeight[i][j]=Double.parseDouble(getTagValue("Row_"+i+"_"+j, element));
	                
	        	   }
	        	  
	           }
	           
	        }
	   }
	 
	    private static String getTagValue(String tag, Element element) {
	        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
	        Node node = (Node) nodeList.item(0);
	        return node.getNodeValue();
	    }


		public double getNetError() {
			return NetError;
		}

		public void setNetError(double netError) {
			NetError = netError;
		}

		public String[] getInputNames() {
			return InputNames;
		}


		public void setInputNames(String[] inputNames) {
			InputNames = inputNames;
		}


		public String[] getOutputsName() {
			return OutputsName;
		}


		public void setOutputsName(String[] outputsName) {
			OutputsName = outputsName;
		}


		public double[] getMinData() {
			return MinData;
		}


		public void setMinData(double[] minData) {
			MinData = minData;
		}


		public double[] getMaxData() {
			return MaxData;
		}


		public void setMaxData(double[] maxData) {
			MaxData = maxData;
		}


		public double[][] getInputHiddenWeight() {
			return InputHiddenWeight;
		}


		public void setInputHiddenWeight(double[][] inputHiddenWeight) {
			InputHiddenWeight = inputHiddenWeight;
		}


		public double[][] getHiddenOutputWeight() {
			return HiddenOutputWeight;
		}


		public void setHiddenOutputWeight(double[][] hiddenOutputWeight) {
			HiddenOutputWeight = hiddenOutputWeight;
		}
	 
	}

