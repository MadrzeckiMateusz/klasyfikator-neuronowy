package Dialogs;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;




import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

import Window.MainWindow;

public class NewNetworkDialog extends JDialog implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Dimension DimensionOfFrame;
	private Dimension DimensionOfScreenSize;
	private int NetworkInputs;
	private int NetworkOutputs;
	private double NetworkLearningRate;
	private JLabel LInputs;
	private JLabel LOutputs;
	private JLabel LLerningRate;
	private JTextField TInputs;
	private JTextField TOutputs;
	private JTextField TLerningRate;
	private JButton	OkButton;
	private JButton SaveButton;
	private JButton closeButton;
	private boolean isSave;
	private boolean isCreate;
	
	
	public NewNetworkDialog(){
		this.isSave = false;
		this.isCreate = false;
		try {
			UIManager.setLookAndFeel
			("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			} catch (Exception e) {
			}
		
		this.DimensionOfFrame = new Dimension(335,220);		
		this.DimensionOfScreenSize = Toolkit.getDefaultToolkit().getScreenSize();
		if (DimensionOfFrame.height > this.DimensionOfScreenSize.height){
			DimensionOfFrame.height = DimensionOfScreenSize.height;
		}
		if (DimensionOfFrame.width > DimensionOfScreenSize.width){
			DimensionOfFrame.width = DimensionOfScreenSize.width;
		}
		setTitle("Nowa Sie�");
		setSize(DimensionOfFrame);
		setLocation((DimensionOfScreenSize.width - DimensionOfFrame.width) / 2, 
			      (DimensionOfScreenSize.height - DimensionOfFrame.height) / 2);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setIconImage(new ImageIcon(NewNetworkDialog.class.getResource("/resources/newfile24.png")).getImage());
		setModal(true);
		setLayout(null);
		CreateGui();
		
		
	}
	public void CreateGui(){
		
		this.LInputs = new JLabel("Liczba Wej��");
		this.LInputs.setBounds(30,25,100,25);
		this.LInputs.setHorizontalAlignment(JLabel.RIGHT);
		this.TInputs = new JTextField();
		this.TInputs.setBounds(140, 25, 90, 25);
		this.LOutputs = new JLabel("Liczba Wyj��");
		this.LOutputs.setHorizontalAlignment(JLabel.RIGHT);
		this.LOutputs.setBounds(30, 60, 100, 25);
		this.TOutputs= new JTextField();
		this.TOutputs.setBounds(140	, 60, 90, 25);
		this.LLerningRate = new JLabel("Wsp. uczenia");
		this.LLerningRate.setHorizontalAlignment(JLabel.RIGHT);
		this.LLerningRate.setBounds(30, 95, 100, 25);
		this.TLerningRate = new JTextField();
		this.TLerningRate.setBounds(140, 95, 90, 25);
		
		
		this.OkButton = new JButton("Ok",new ImageIcon(NewNetworkDialog.class.getResource("/resources/ok16.png")));
		this.OkButton.setBounds(5, 140, 100, 25);
		this.OkButton.addActionListener(this);
		this.SaveButton = new JButton("Zastosuj",new ImageIcon(NewNetworkDialog.class.getResource("/resources/save16.png")));
		this.SaveButton.setBounds(110,140,100,25);
		this.SaveButton.addActionListener(this);
		this.closeButton = new JButton("Anuluj",new ImageIcon(NewNetworkDialog.class.getResource("/resources/cancel16.png")));
		this.closeButton.setBounds(215, 140, 100, 25);
		this.closeButton.addActionListener(this);
		
		add(this.LInputs);
		add(this.LOutputs);
		add(this.LLerningRate);
		add(this.TInputs);
		add(this.TOutputs);
		add(this.TLerningRate);
		add(this.OkButton);
		add(this.SaveButton);
		add(this.closeButton);
	}
	public void SetNetworkSize(){
		try{
			this.NetworkInputs =  Integer.parseInt(this.TInputs.getText());
			this.NetworkOutputs = Integer.parseInt(this.TOutputs.getText());
			this.NetworkLearningRate = Double.parseDouble(this.TLerningRate.getText());
		}
		catch (Exception e){
			JOptionPane.showMessageDialog(null,"B��dna warto��");
		}
		
		
	}
	public void Clearfield(){
		this.TInputs.setText(null);
		this.TOutputs.setText(null);
		this.TLerningRate.setText(null);
	}
	
	public void SetVisiableDialog(){
		setVisible(true);
	}
	public int getNetworkInputs() {
		return NetworkInputs;
	}
	public boolean isCreate() {
		return isCreate;
	}
	public void setCreate(boolean isCreate) {
		this.isCreate = isCreate;
	}
	public void setNetworkInputs(int networkInputs) {
		NetworkInputs = networkInputs;
	}
	public int getNetworkOutputs() {
		return NetworkOutputs;
	}
	public void setNetworkOutputs(int networkOutputs) {
		NetworkOutputs = networkOutputs;
	}
	
	
	public double getNetworkLearningRate() {
		return NetworkLearningRate;
	}
	public void setNetworkLearningRate(double networkLearningRate) {
		NetworkLearningRate = networkLearningRate;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object s = e.getSource();
		
		if (s == this.OkButton){
			if (this.isSave == false){
				SetNetworkSize();
				setVisible(false);
				Clearfield();
				this.isCreate = true;
				this.isSave = false;
			}
			else if (this.isSave == true){
				setVisible(false);
				Clearfield();
				this.isCreate = true;
				this.isSave= false;
			}
			
		}
		else if(s == this.closeButton){
			this.NetworkInputs = 0;
			this.NetworkOutputs = 0;
			this.NetworkLearningRate = 0.0;
			this.isCreate = false;
			Clearfield();
			setVisible(false);
		}
		else if(s == this.SaveButton){
			this.isSave=true;
			SetNetworkSize();
			
			this.isCreate = true;
		}
		
	}
}
