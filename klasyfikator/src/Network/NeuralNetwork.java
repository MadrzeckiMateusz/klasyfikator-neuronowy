package Network;

import java.util.Random;

public class NeuralNetwork {
	
	

	private int num_Inputs;
	private int num_Hidden;
	private int num_Outputs;
	private double [][] input_hidden_layer_weight;
	private double [][] hidden_output_layer_weight;
	private double [] inputs;
	private double [] hidden_neurons;
	private double [] hidden_neurons_deritive;
	private double [] outputs_neurons;
	private double [] outputs_neurons_deritive;
	private double [] delta_hidden;
	private double [] delta_outputs;
	private double learningRate;
	private Random generator = new Random();
	
	
	private double [] TrainingVector;
	private int [] CorectAnswer;
	
	
	public NeuralNetwork(int num_inputs, int num_outputs, double lerningRate){
		this.num_Hidden = (int) Math.sqrt(num_outputs*num_inputs)+1;
		this.num_Inputs = num_inputs;
		this.num_Outputs = num_outputs;
		this.input_hidden_layer_weight = new double[num_inputs + 1][this.num_Hidden];
		this.hidden_output_layer_weight = new double[this.num_Hidden + 1][num_outputs];
		this.inputs = new double[num_inputs +1];
		this.hidden_neurons = new double[this.num_Hidden + 1];
		this.hidden_neurons_deritive = new double[this.num_Hidden +1];
		this.outputs_neurons = new double[num_outputs];
		this.outputs_neurons_deritive = new double[num_outputs];
		this.delta_hidden = new double[this.num_Hidden+1];
		this.delta_outputs = new double[num_outputs];
		this.TrainingVector = new double[num_inputs];
		this.CorectAnswer = new int[num_outputs];
		
		this.learningRate = lerningRate;
		
	}
	
	public void InitializeWeight(){
		this.inputs[this.num_Inputs] = 1.0;
		this.hidden_neurons[this.num_Hidden] = 1.0;
		
		for (int i = 0; i < this.num_Inputs + 1; i++){
			for(int j = 0; j < this.num_Hidden; j++){
				this.input_hidden_layer_weight[i][j] = (this.generator.nextDouble()-0.5);
			}
		}
		
		for (int i = 0; i < this.num_Hidden + 1; i++){
			for (int j = 0; j < this.num_Outputs; j++){
				this.hidden_output_layer_weight[i][j] = (this.generator.nextDouble()- 0.5);
			}
		}
	}
	
	
	public void P_foward(double [] allowinput){
		
		for (int i = 0; i < this.num_Inputs; i++){
			this.inputs[i] = allowinput[i];
		}
		
		for (int i = 0; i < this.num_Hidden; i++){
			double sum = 0.0;
			for (int j = 0; j < this.num_Inputs + 1; j++){
				
				sum += this.inputs[j] * this.input_hidden_layer_weight[j][i];
			}
			this.hidden_neurons[i]= ActivationFun.SigmoidalFun(sum);
			
		}
		for (int i = 0 ; i < this.num_Outputs; i++){
			
			double sum = 0.0;
			for (int j = 0; j < this.num_Hidden + 1; j++){
				
				sum += this.hidden_neurons[j] * this.hidden_output_layer_weight[j][i];
			}
			this.outputs_neurons[i] = ActivationFun.SigmoidalFun(sum);
			
		}
	}
	
	public void BackProp(double[] target){
		for (int i = 0; i < this.num_Outputs; i++){
			this.outputs_neurons_deritive[i] = ActivationFun.Deritive(this.outputs_neurons[i]);
			this.delta_outputs[i]= (target[i]-this.outputs_neurons[i])*this.outputs_neurons_deritive[i];
		}
		for (int i = 0; i < this.num_Hidden +1; i++){
			this.hidden_neurons_deritive[i]= ActivationFun.Deritive(this.hidden_neurons[i]);
			double delta_sum = 0.0;
			for (int j = 0; j < this.num_Outputs; j++){
				delta_sum += this.hidden_output_layer_weight[i][j] * this.delta_outputs[j];
			}
			this.delta_hidden[i] = this.hidden_neurons_deritive[i] * delta_sum;
		}
		
		for (int i = 0; i < this.num_Outputs; i++){
			for (int j = 0; j < this.num_Hidden + 1; j++){
				this.hidden_output_layer_weight[j][i] += this.learningRate * this.delta_outputs[i]*this.hidden_neurons[j];
			}
		}
		for (int i = 0; i < this.num_Hidden; i++){
			for (int j = 0; j < this.num_Inputs + 1; j++){
				this.input_hidden_layer_weight[j][i] += this.learningRate * this.delta_hidden[i] * this.inputs[j];
			}
		}
		
	}
	
public void P_foward_reco(double [] allowinput){
		
		for (int i = 0; i < this.num_Inputs; i++){
			this.inputs[i] = allowinput[i];
		}
		double [][] copy_input_hidden_layer= this.input_hidden_layer_weight;
		double [][] copy_hidden_output_layer= this.hidden_output_layer_weight;
				for (int i = 0; i < this.num_Hidden; i++){
			double sum = 0.0;
			for (int j = 0; j < this.num_Inputs + 1; j++){
				
				sum += this.inputs[j] * this.input_hidden_layer_weight[j][i];
			}
			this.hidden_neurons[i]= ActivationFun.SigmoidalFun(sum);
			
		}
		for (int i = 0 ; i < this.num_Outputs; i++){
			
			double sum = 0.0;
			for (int j = 0; j < this.num_Hidden + 1; j++){
				
				sum += this.hidden_neurons[j] * this.hidden_output_layer_weight[j][i];
			}
			this.outputs_neurons[i] = ActivationFun.SigmoidalFun(sum);
			
		}
		this.hidden_output_layer_weight = copy_hidden_output_layer;
		this.input_hidden_layer_weight = copy_input_hidden_layer;
	}
	public double[] getTrainingVector() {
		return TrainingVector;
	}

	public void setTrainingVector(double[] trainingVector) {
		TrainingVector = trainingVector;
	}

	public int[] getCorectAnswer() {
		return CorectAnswer;
	}

	public void setCorectAnswer(int[] corectAnswer) {
		CorectAnswer = corectAnswer;
	}

	public int getNum_Inputs() {
		return num_Inputs;
	}
	public void setNum_Inputs(int num_Inputs) {
		this.num_Inputs = num_Inputs;
	}
	public int getNum_Hidden() {
		return num_Hidden;
	}
	public void setNum_Hidden(int num_Hidden) {
		this.num_Hidden = num_Hidden;
	}
	public int getNum_Outputs() {
		return num_Outputs;
	}
	public void setNum_Outputs(int num_Outputs) {
		this.num_Outputs = num_Outputs;
	}
	public double[][] getInput_hidden_layer_weight() {
		return input_hidden_layer_weight;
	}
	public void setInput_hidden_layer_weight(double[][] input_hidden_layer_weight) {
		this.input_hidden_layer_weight = input_hidden_layer_weight;
	}
	public double[][] getHidden_output_layer_weight() {
		return hidden_output_layer_weight;
	}
	public void setHidden_output_layer_weight(double[][] hidden_output_layer_weight) {
		this.hidden_output_layer_weight = hidden_output_layer_weight;
	}
	public double[] getInputs() {
		return inputs;
	}
	public void setInputs(double[] inputs) {
		this.inputs = inputs;
	}
	public double[] getHidden_neurons() {
		return hidden_neurons;
	}
	public void setHidden_neurons(double[] hidden_neurons) {
		this.hidden_neurons = hidden_neurons;
	}
	public double[] getOutputs_neurons() {
		return outputs_neurons;
	}
	public void setOutputs_neurons(double[] outputs_neurons) {
		this.outputs_neurons = outputs_neurons;
	}
	public double getLearningRate() {
		return learningRate;
	}
	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}
	
	
}
