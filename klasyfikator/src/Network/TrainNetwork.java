package Network;

import Data.DataLoad;
import Data.DataNorm;
import Data.RandomUnique;

public class TrainNetwork {
		private NeuralNetwork network;
		private DataLoad dataLoad;
		private DataNorm dataNorm;
		private RandomUnique random;
		private double ErrorNetwork;
		private int NetworkInput;
		private int NetworkOutput;
		private int NetworkHiddenNeuron;
		private double[] MaxValue;
		private double[] MinValue;
		private double[][] InputHiddenWeight;
		private double[][] HiddenOutputWeight;
		private int IDNet;
		public TrainNetwork(int input, int output, double lerningRate){
			this.network = new NeuralNetwork(input, output, lerningRate);
			this.dataLoad = null;
			this.dataNorm = new DataNorm();
			this.random = null;			
			this.IDNet = 0;
			this.ErrorNetwork = 0.0;
			this.HiddenOutputWeight = null;
			this.InputHiddenWeight = null;
			this.NetworkInput = this.network.getNum_Inputs();
			this.NetworkOutput = this.network.getNum_Outputs();
			this.NetworkHiddenNeuron = this.network.getNum_Hidden();
		}
		public void LoadAndNormData(String plik){
			this.network.InitializeWeight();
			this.dataLoad = new DataLoad();
			this.dataLoad.ReadData(plik);
			this.dataNorm = new DataNorm(this.dataLoad.getLkolumn(),this.dataLoad.getLwierszy());
			this.dataNorm.NormalizeData(this.dataLoad.getDane(),network.getNum_Inputs());
			this.dataNorm.DataTraining(this.dataNorm.getDaneNorm(), network.getNum_Outputs());
			
			this.random = new RandomUnique(this.dataNorm.getLVectorTraining()-1);
			this.MaxValue = this.dataNorm.getMaxValue();
			this.MinValue = this.dataNorm.getMinValue();
			
		}
		public void SetNetworkWeight(double[][] InputHidden, double[][] HiddenOutput){
			this.network.setHidden_output_layer_weight(HiddenOutput);
			this.network.setInput_hidden_layer_weight(InputHidden);
		}
		public void SetNetworkmaxMin(double[] maxdata, double[] mindata){
			this.dataNorm.setMaxValue(maxdata);
			this.dataNorm.setMinValue(mindata);
		}
		
		public double[] getMaxValue() {
			return MaxValue;
		}
		public void setMaxValue(double[] maxValue) {
			MaxValue = maxValue;
		}
		public double[] getMinValue() {
			return MinValue;
		}
		public void setMinValue(double[] minValue) {
			MinValue = minValue;
		}
		public double TrainingNetwork(){
			this.ErrorNetwork = 0.0;
			for(int k=1; k<1000;k++){
				this.random.init();	
			
				for(int j=0;j<this.dataNorm.getLVectorTraining()-1;j++){
			
					int rand = 	random.get();
					
					double[] tmpV = this.dataNorm.SetTrainingVector(this.dataNorm.getDaneTrain(),rand,network.getNum_Inputs());
					double[] answer = this.dataNorm.SetCorectAnwer(this.dataNorm.getDaneTrain(),rand,network.getNum_Outputs());
				
		
					network.P_foward(tmpV);
					network.BackProp(answer);
				}
				
			}
			this.InputHiddenWeight = network.getInput_hidden_layer_weight();
			this.HiddenOutputWeight = network.getHidden_output_layer_weight();
			for(int x=0; x<this.dataNorm.getLCheckVector();x++)
			{
				double[] tmpC = this.dataNorm.SetTrainingVector(this.dataNorm.getDataCheck(),x,network.getNum_Inputs());
				double[] answerC = this.dataNorm.SetCorectAnwer(this.dataNorm.getDataCheck(),x,network.getNum_Outputs());
				network.P_foward(tmpC);
				double [] outputNeu = this.network.getOutputs_neurons();
				for(int z=0; z<this.network.getNum_Outputs();z++)
				{
					this.ErrorNetwork += ((Math.abs(  answerC[z]-outputNeu[z]))/network.getNum_Outputs());
				}
			}
			
			return this.ErrorNetwork;
		}
		public double[] Recognize(double[] vector){
			network.P_foward_reco(dataNorm.NormalizeVector(vector,network.getNum_Inputs()));
			return network.getOutputs_neurons();
		}
		public double getErrorNetwork() {
			return ErrorNetwork;
		}
		public void setErrorNetwork(double errorNetwork) {
			ErrorNetwork = errorNetwork;
		}
		public double[][] getInputHiddenWeight() {
			return InputHiddenWeight;
		}
		public void setInputHiddenWeight(double[][] inputHiddenWeight) {
			InputHiddenWeight = inputHiddenWeight;
		}
		public double[][] getHiddenOutputWeight() {
			return HiddenOutputWeight;
		}
		public void setHiddenOutputWeight(double[][] hiddenOutputWeight) {
			HiddenOutputWeight = hiddenOutputWeight;
		}
		public int getNetworkHiddenNeuron() {
			return NetworkHiddenNeuron;
		}
		public void setNetworkHiddenNeuron(int networkHiddenNeuron) {
			NetworkHiddenNeuron = networkHiddenNeuron;
		}
		public int getIDNet() {
			return IDNet;
		}
		public void setIDNet(int iDNet) {
			IDNet = iDNet;
		}
		public int getNetworkInput() {
			return NetworkInput;
		}
		public void setNetworkInput(int networkInput) {
			NetworkInput = networkInput;
		}
		public int getNetworkOutput() {
			return NetworkOutput;
		}
		public void setNetworkOutput(int networkOutput) {
			NetworkOutput = networkOutput;
		}


}
